import React from 'react'
import './WeatherPlaceholder.css'

function WeatherPlaceholder () {
  return (
        <div className="plugBlock">
            <div className="plugBlock__img" />
            <p className="plugBlock__text">Fill in all the fields and the weather will be displayed</p>
        </div>
  )
}

export default WeatherPlaceholder
