/**
 *  function analogue Array.prototype.filter
 * @template T
 * @param {function(element: T) : boolean} predicate
 * @param {T[]} collection
 * @return {T[]}
 */
function filter (predicate, collection) {
  return collection.reduce((result, item) => {
    if (predicate(item)) result.push(item)
    return result
  }, [])
}