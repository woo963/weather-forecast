/**
 *  function analogue Array.prototype.map
 * @template T, V
 * @param {function(element: T) : V} callback
 * @param {T[]} collection
 * @return {V[]}
 */
function map (callback, collection) {
  return collection.reduce((result, item) => [...result, callback(item)], [])
}